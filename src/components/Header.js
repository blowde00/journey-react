import React from 'react'
import {Link} from 'react-router-dom'

export default function Header() {
  return (
    <div>
      <nav>
        <ul>
          <li><Link to='/journey'>Plan Your Journey</Link></li>
          <li><Link to='/journey/reviews'>Journey Reviews</Link></li>
          <li><Link to='/journey/about'>About Your Guides</Link></li>
        </ul >
      </nav >
    </div >
  )
}