import React from 'react'
import {Link} from 'react-router-dom'

export default function Footer() {
  return (
    <footer>
        <p>Copyright 2022, Journey Unlimited LLC</p>
        <Link to='/'>Start a new Journey</Link>
    </footer>
  )
}