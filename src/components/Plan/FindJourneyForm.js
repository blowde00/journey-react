import React from 'react'
import States from './States';

export default function FindJourneyForm() {
  const STATES = {
    AL: 'Alabama', AK: 'Alaska', AZ: 'Arizona', AR: 'Arkansas', CA: 'California', CO: 'Colorado', CT: 'Connecticut', DE: 'Delaware', FL: 'Florida', GA: 'Georgia', HI: 'Hawaii', ID: 'Idaho', IL: 'Illinois', IN: 'Indiana', IA: "Iowa", KS:
      'Kansas', KY: 'Kentucky', LA: 'Louisiana', ME: 'Maine', MD: 'Maryland', MA: "Massachusetts", MI: 'Michigan', MN: 'Minnesota', MS: 'Mississippi', MO: 'Missouri',
    MT: 'Montana', NE: 'Nebraska', NV: 'Nevada', NH: 'New Hampshire', NJ: 'New Jersey', NM: 'New Mexico', NY: 'New York', NC: 'North Carolina', ND: 'North Dakota', OH: 'Ohio', OK: 'Oklahoma', OR: 'Oregon', PA: 'Pennsylvania', RI: 'Rhode Island',
    SC: 'South Carolina', SD: 'South Dakota', TN: 'Tennessee', TX: 'Texas', UT: 'Utah', VT: 'Vermont', VA: 'Virginia', WA: 'Washington', WV: 'West Virginia', WI: 'Wisconsin', WY: 'Wyoming'
  };
  
  const optionValues = Object.keys(STATES)
  
  //Math.floor(Math.random() * 100000)

  return (
    <form>
      <h3>Your Journey Awaits...</h3>
      <div id="search_row">
        <label htmlFor="state">Choose a state:</label>
        <select id="state" name="state">
          <option value="">All</option>
          {optionValues.map(item => {return <States key={item} value={item} displayName={STATES[item]} />})}
        </select>

        <label htmlFor="activity">Activity:</label>
        <select id="activity" name="activityId">
          <option value="5">Biking</option>
          <option value="9">Camping</option>
          <option value="7">Climbing</option>
          <option value="11">Fishing</option>
          <option value="14">Hiking</option>
          <option value="15">Horseback Riding</option>
          <option value="20">Picnicking</option>
          <option value="104">Photography</option>
          <option value="106">Swimming</option>
          <option value="25">Water Sports</option>
          <option value="26">Wildlife Viewing</option>
        </select>

        <label htmlFor="limit">Number of Results:</label>
        <select id="limit" name="limit">
          <option value="5">5</option>
          <option value="10">10</option>
          <option value="50">50</option>
          <option value="999">All</option>
        </select>

        <button type="button" id="submit_button">Search Journey Destinations</button>
      </div>
    </form>
  )
}