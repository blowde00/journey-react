import React, {useState} from 'react'
import JourneyContext from '../../context/JourneyContext'
import JourneyResultsContainer from './JourneyResultsContainer'
import FindJourneyForm from './FindJourneyForm'


export default function PlanYourJourney() {
  const [state, setState] = useState({});

  return (
    <JourneyContext.Provider value={[state, setState]}>
      <FindJourneyForm />
      <JourneyResultsContainer />
    </JourneyContext.Provider>
  )
}