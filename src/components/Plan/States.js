import React from 'react'

export default function States({value, displayName}) {
  return (
    <option value={value}>{displayName}</option>
  )
}
