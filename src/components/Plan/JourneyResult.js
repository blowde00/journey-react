import React from 'react'

export default function JourneyResult() {
  return (
    <tr>
      <td>Search Result A</td>
      <td>123-456-7890</td>
      <td>email.com</td>
      <td><button>Map It!</button></td>
    </tr>
  )
}