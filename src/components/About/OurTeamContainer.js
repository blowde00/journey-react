import React from 'react'
import TeamMember from './TeamMember'

export default function OurTeamContainer() {
  const team = [{
    id: 'alan',
    fullName: 'Alan Andersen',
    Title: 'Chief Innovation Officer (CIO)',
    About: 'Alan graduated from the University of Nebraska Lincoln with a B.S. in Anthropology and from Arizona State Univerity with an M.S. in Business Analytics. He was previously a Technology Analyst at State Farm and hopes to transition to a software developer role in the near future in addition to his current duties as CIO.',
    Email: 'alan.andersen.ernm@statefarm.com',
    imgPath: ''
  },
  {
    id: 'brandon',
    fullName: 'Brandon Lowdermilk',
    Title: 'Chief Operating Officer (COO)',
    About: 'Brandon earned his B.S. in Organizational Leadership from the University of Arkansas - Fort Smith. He has served as the COO of Journey Unlimited LLC since 1978. In his spare time, Brandon enjoys long walks on the beach, extreme couponing, and time travel. He is currently brushing up on his software development skills to plan for the future of the company.',
    Email: 'brandon.lowdermilk.wyp1@statefarm.com',
    imgPath: ''
  },
  {
    id: 'josh',
    fullName: 'Josh Martin',
    Title: 'Janitor',
    About: 'Josh has come a long way in his career. Starting as Junior Janitor in 2019, he made his way to Assistant to the Regional Janitor in late 2020, and finally to made it official by becoming Janitor in 2022. He has his sights set on someday being Lead Janitor of his division. In his spare time Josh likes to sharpen up his web-developing skills, so much so that he has found his way into an honorary role on the executive board of the Journey corp. Josh, his two kids, and his wife are all from Normal, IL. He enjoys hiking outside, visiting national parks, and playing outdoor sports. His favorite hike was through the backcountry of Zion National Park.',
    Email: 'josh.martin.dmth@statefarm.com',
    imgPath: ''
  },
  {
    id: 'brendan',
    fullName: 'Brendan Streitmatter',
    Title: 'Chief Financial Officer (CFO)',
    About: 'Brendan graduated from Illinois State University in May of 2020 with degrees in both Finance and Insurance. This background led to his selection as CFO for our prestigious company. In his prior role Brendan worked as a Technology Analyst at State Farm.',
    Email: 'brendan.streitmatter.ycfq@statefarm.com',
    imgPath: ''
  }]


  return (
    <main>
      <h2>Our Team</h2>
      <section>
        {team.map(person => {return <TeamMember key={person.id} person={person}/>})}
      </section>
    </main>
  )
}