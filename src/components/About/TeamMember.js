import React from 'react'

export default function TeamMember({person}) {
  return (
    <div>
      <article>
        <div id={person.id}></div>
        <div>
          <h3>{person.fullName}</h3>
          <p className="title">{person.Title}</p>
          <p className="description">{person.About}</p>
          <p>{person.Email}</p>
        </div>
      </article>

    </div>
  )
}