import React from 'react';
import { Routes, Route } from 'react-router-dom';
import LandingPage from './LandingPage';
import PlanYourJourney from './Plan/PlanYourJourney';
import JourneyReviews from './Review/JourneyReviews';
import About from './About/About';
import App from '../App';

export default function Router() {
    return (
        <Routes>
            <Route path="/" element={<LandingPage />}></Route>
            <Route path="journey" element={<App />}>
                <Route path="" element={<PlanYourJourney />}></Route>
                <Route path="reviews" element={<JourneyReviews />}></Route>
                <Route path="about" element={<About />}></Route>
            </Route>
            
        </Routes>
    )
}