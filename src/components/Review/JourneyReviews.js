import React, {useState} from 'react'
import ReviewContext from '../../context/ReviewContext'
import ReviewContainer from './ReviewContainer'
import ReviewForm from './ReviewForm'

const initial = [
  {
    recArea: 'Warm Springs Wilderness',
    review: "Absolutely stunning!",
    rating: '⭐⭐⭐⭐'
  },
  {
    recArea: 'Angelese National Forest',
    review: "One of the best national forests, this one harbors such wonders as Switzer's, Trail canyon, Fire Camps, Nike Missile Bases, and many more.",
    rating: '⭐⭐⭐⭐⭐'
  },
  {
    recArea: "O'ahu Forest National Wildlife Refuge",
    review: "This place is a remote location in the ko'olau moutain range that is actually closed to the public.",
    rating: '⭐⭐⭐'
  }
]

export default function JourneyReviews() {
  const [state, setState] = useState(initial);
  
  return (
    <ReviewContext.Provider value={[state, setState]}>
      <div>JourneyReviews</div>
      <ReviewContainer/>
      <ReviewForm/>
    </ReviewContext.Provider>
  )
}