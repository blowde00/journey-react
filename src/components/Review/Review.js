import React from 'react'


export default function Review({ review }) {
  return (
    <tr>
      <td>{review.recArea}</td>
      <td>{review.review}</td>
      <td>{review.rating}</td>
    </tr>
  )
}