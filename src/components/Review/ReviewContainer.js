import React, { useContext } from 'react'
import Review from './Review'
import ReviewContext from '../../context/ReviewContext';

export default function ReviewContainer() {
  const [state, setState] = useContext(ReviewContext);

  return (
    <section id="reviews">
      <table>
        <thead>
          <tr>
            <th id="recreationHeader">Recreation Area</th>
            <th>Review</th>
            <th id="ratingHeader">Rating</th>
          </tr>
        </thead>
        <tbody id="blog_posts">
          {state.map(review => { return <Review key={Math.floor(Math.random() * 100000)} review={review} /> })}
        </tbody>
      </table>
    </section>
  )
}