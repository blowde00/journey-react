import React from 'react'
import {Link} from 'react-router-dom'

//need to figure out how to send 

export default function LandingPage() {
  return (
    <Link to='/journey'>
      <article>
        <h1>Your Journey Begins Here...</h1>
      </article>
    </Link>
  )
}